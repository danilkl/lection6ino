//
//  Model.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 03/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Model.h"
#import "Item.h"

@implementation Model

- (instancetype)init {
    self = [super init];
    if (self) {
        _items = [[NSMutableArray alloc] init];
        [self load];
    }
    return self;
}

- (void)save {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.items];
    
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    [data writeToFile:[path stringByAppendingPathComponent:@"MyFile"] atomically:YES];
}

- (void)load {
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    NSData *data = [NSData dataWithContentsOfFile:[path stringByAppendingPathComponent:@"MyFile"]];
    
    if (data) {
        self.items = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
}

@end
